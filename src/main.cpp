#include "Arduino.h"
#include "PubSubClient.h"
#include "SPI.h"
#include <U8x8lib.h>
#include <WiFi.h>
#include "Wire.h"

#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16
#define OLED_ADDR 0x3c

#define SOIL_METER_PIN 34

#define MUX_PIN_S0 12
#define MUX_PIN_S1 14
#define MUX_PIN_S2 27
#define MUX_PIN_S3 26

#define TOTAL_CHANNELS 2

// These were measured with the sensor held in air, and inside a glass of water.
const unsigned int sensor_dry = 3475;
const unsigned int sensor_wet = 1695;

const char* wifi_ssid = "internet-of-shit";
const char* wifi_pass = "";

const char* mqtt_host = "10.1.2.1";
const char* mqtt_user = "gardener";
const char* mqtt_pass = "";

const unsigned long wifi_retry_interval = 30 * 1000;
const unsigned long mqtt_retry_interval = 5 * 1000;

const unsigned long timezone_seconds = -3 * 3600;

unsigned long long last_mqtt_retry = 0;
unsigned long long last_wifi_retry = 0;

int current_moisture = 0;
int current_channel = 0;

WiFiClient wifi_client;
PubSubClient mqtt_client(wifi_client);
U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(OLED_SCL, OLED_SDA, OLED_RST);

bool wifi_connected () {
  return WiFi.status() == WL_CONNECTED;
}

void wifi_reconnect () {
  bool force_reconnect = last_wifi_retry == 0;
  if ( force_reconnect || ( ! wifi_connected() && ( ( millis() - last_wifi_retry ) > wifi_retry_interval ) ) ) {
    last_wifi_retry = millis();
    Serial.printf("Connecting to wifi network %s ", wifi_ssid);
    unsigned short count = 0;
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(wifi_ssid, wifi_pass);
    while ( ! wifi_connected() && ( count < 30 ) ) {
      count++;
      delay(1000);
      Serial.print(" ... ");
    }
    if ( wifi_connected() ) {
      const unsigned short dst_secs = 0;
      configTime(timezone_seconds, dst_secs, "10.1.2.1", "pool.ntp.br", "pool.ntp.org");
      Serial.printf(" connected with address %s \n", WiFi.localIP().toString().c_str());
    } else {
      Serial.println(" fail. Will retry soon.");
    }
  }
}

void mqtt_publish(String subtopic, String value) {
  String topic;
  if (subtopic == "") {
    topic = "gardener";
  } else {
    topic = "gardener/" + subtopic;
  }
  unsigned int buffer_size_needed = topic.length() + value.length() + 10;
  if (mqtt_client.getBufferSize() < buffer_size_needed) {
    if (!mqtt_client.setBufferSize(buffer_size_needed)) {
      Serial.printf("Failed setting MQTT buffer size from %d to %d. ", mqtt_client.getBufferSize(),              buffer_size_needed);
    }
  }
  if (mqtt_client.connected() && (mqtt_client.getBufferSize() >= buffer_size_needed)) {
    if (mqtt_client.publish(topic.c_str(), value.c_str()), true) {
      Serial.printf("\t\tPublished %s to topic %s\n", value.c_str(), topic.c_str());
    } else {
      Serial.printf("Failure publishing %s to topic %s\n", value.c_str(), topic.c_str());
    }
  } else {
    Serial.printf("\t\tNot publishing %s to topic %s , rc=%d", value.c_str(), topic.c_str(), mqtt_client.        state());
  }
  if (mqtt_client.getBufferSize() > 128) {
    mqtt_client.setBufferSize(128);
  }
}

void mqtt_reconnect() {
  bool force_reconnect = last_mqtt_retry == 0;
  if ( ( !mqtt_client.connected() && ( ( millis() - last_mqtt_retry ) > mqtt_retry_interval ) ) || force_reconnect ) {
    last_mqtt_retry = millis();
    Serial.print("Connecting to MQTT broker at ");
    Serial.print(mqtt_host);
    Serial.print(" ... ");
    String client_id = "gardener-" + String(random(0xffff), HEX);
    // client-id, username, password, will{topic, qos, retain, message}
    mqtt_client.setServer(mqtt_host, 1883);
    if (mqtt_client.connect(client_id.c_str(), mqtt_user, mqtt_pass, "gardener/available", 0, true, "false")) {
      Serial.println(" connected");
    } else {
      Serial.print("failed, rc=");
      Serial.println(mqtt_client.state());
    }
    mqtt_publish("available", "true");
  }
}

void led (bool on) {
  digitalWrite(LED_BUILTIN, on ? HIGH : LOW);
}

void setup () {
  pinMode(LED_BUILTIN, OUTPUT);
  led(true);

  pinMode(MUX_PIN_S0, OUTPUT);
  pinMode(MUX_PIN_S1, OUTPUT);
  pinMode(MUX_PIN_S2, OUTPUT);
  pinMode(MUX_PIN_S3, OUTPUT);

  Serial.begin(115200);

  u8x8.begin();
  u8x8.clearDisplay();
  u8x8.setFont(u8x8_font_artossans8_r);
  u8x8.drawString(0, 1, "Starting up ...");

  wifi_reconnect();

  mqtt_reconnect();

  u8x8.setInverseFont(1);
  u8x8.drawString(0, 3, "done");
  u8x8.setInverseFont(0);

  u8x8.clearDisplay();
  led(false);
}

void draw () {
  char line[16] = "";

  const char smiley_ok[3] = {':', ')', '\0'};
  const char smiley_nok[3] = {':', '(', '\0'};

  sprintf(line, "Channel %d", current_channel);

  u8x8.setFont(u8x8_font_artossans8_r);
  u8x8.drawString(0, 0, line);

  sprintf(line, "%3d%%", current_moisture);

  u8x8.setFont(u8x8_font_inr21_2x4_f);
  u8x8.drawString(6, 1, line);

  u8x8.setInverseFont(1);

  u8x8.setFont(u8x8_font_artossans8_r);

  sprintf(line, "%s WIFI", wifi_connected() ? smiley_ok : smiley_nok );
  u8x8.drawString(0, 5, line);

  sprintf(line, "%s MQTT", mqtt_client.connected() ? smiley_ok : smiley_nok );
  u8x8.drawString(8, 5, line);

  u8x8.setInverseFont(0);

  struct tm* time_info;
  time_t now = time(nullptr);
  time_info = localtime(&now);
  sprintf(line, "%02d/%02d %02d:%02d:%02d", time_info->tm_mday, time_info->tm_mon + 1, time_info->tm_hour, time_info->tm_min, time_info->tm_sec);

  u8x8.setFont(u8x8_font_artossans8_r);
  u8x8.drawString(0, 6, line);

  sprintf(line, "%15s", WiFi.localIP().toString().c_str());
  u8x8.setFont(u8x8_font_artossans8_r);
  u8x8.drawString(0, 7, line);
}

void mux_switch( int channel ) {
  // channel must be from 0 to 15 representing the analog channel on the multiplexor
	// MUX_PIN_S0 through MUX_PIN_S3 are the Arduino pins connected to the selector pins of this board.
	digitalWrite(MUX_PIN_S3, HIGH && (channel & B00001000));
	digitalWrite(MUX_PIN_S2, HIGH && (channel & B00000100));
	digitalWrite(MUX_PIN_S1, HIGH && (channel & B00000010));
	digitalWrite(MUX_PIN_S0, HIGH && (channel & B00000001));
}

unsigned int read_sensor ( int channel ) {
  mux_switch(channel);
  int reading = analogRead(SOIL_METER_PIN);
  Serial.printf("Analog read from %d : %d . \n", SOIL_METER_PIN, reading);
  return reading;
}

unsigned short calculate_moisture ( int reading , int dry_reading , int wet_reading ) {
  int result = map(reading, dry_reading, wet_reading, 0, 100);
  Serial.printf("Calculating moisture from %06d ( dry: %06d / wet: %06d ) : %d%% .\n", reading, dry_reading, wet_reading, result);
  // Filter out results out of 0-100%
  if (result > 100) {
    result = 100;
  }
  if (result < 0) {
    result = 0;
  }
  return result;
}

void loop () {

  if ( last_wifi_retry > millis() ) {
    last_wifi_retry = 0;
  }
  if ( ! wifi_connected() ) {
    wifi_reconnect();
  }

  if ( last_mqtt_retry > millis() ) {
    last_mqtt_retry = 0;
  }
  if ( ! mqtt_client.connected() ) {
    mqtt_reconnect();
  }

  mqtt_client.loop();

  led(true);
  current_channel = ( millis() / ( 1000 * 10 ) ) % TOTAL_CHANNELS;
  current_moisture = calculate_moisture(read_sensor(current_channel), sensor_dry, sensor_wet);
  mqtt_publish("moisture", String(current_moisture));
  led(false);

  draw();

  delay(1000);
}

