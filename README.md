# Gardener: monitors soil moisture

I wanted a platform to monitor the soil moisture on my potted plants. Using the analog capacitive soil humidity meter, I was able to work it out fine, with the countless tutorials over the web.

However, I got more than one plant, so I wanted to monitor all of them. Then I found out about the analog multiplexers for Arduino/ESP8266, which would expand the possibilities to up to 16 sensors! And on a single board.

These are analog sensors, however, and they need calibration. When introducing a great length of wire, you tend to have voltage drop. And also a single ESP32 doing its job is a single point of failure as well. So I am putting this project on hold for now, in favor of a single board at each pot, even if that means it getting a lot more expensive, it still makes for an easier time setting up.
